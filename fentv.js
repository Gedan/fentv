var express = require('express'),
	fs = require('fs'),
	streams = require('./lib/streams.js'),
	chat = require('./lib/chat.js');

//globals
var app = express(),
	server = app.listen(8081),
	io = require('socket.io').listen(server, { log: false });

app.configure(function(){
	app.set('title', 'Fentv');
	app.set('views',__dirname);
	app.set('view engine','jade');
	app.use(express.static(__dirname+'/public'));
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(app.router);
});

app.get('/',function(req,res){
	res.render('index',{title:'Fentv'});
});

var activeUsers = 0; //track active users
var fentv = new streams(io,function(socket){
	activeUsers++;
	io.sockets.emit('active-users',{users:activeUsers});
	var fenchat = new chat(socket);
	socket.on('disconnect',function(){
		activeUsers--;
		io.sockets.emit('active-users',{users:activeUsers});
		if(fenchat.conn!==false){
			fenchat.conn.disconnect('Fentv testing');
		}
	});
});