module.exports=function(version){
	this.getColor = function(message){
		var codes = {
		    white: '\\u00030',
		    black: '\\u00031',
		    dark_blue: '\\u00032',
		    dark_green: '\\u00033',
		    light_red: '\\u00034',
		    dark_red: '\\u00035',
		    magenta: '\\u00036',
		    orange: '\\u00037',
		    yellow: '\\u00038',
		    light_green: '\\u00039',
		    cyan: '\\u000310',
		    light_cyan: '\\u000311',
		    light_blue: '\\u000312',
		    light_magenta: '\\u000313',
		    gray: '\\u000314',
		    light_gray: '\\u000315',
		    reset: '\\u000f',
		},exp,msg=message,code=false;
		for(var index in codes){
			if(codes.hasOwnProperty(index)){
				exp = new RegExp('('+codes[index]+')');
				if(exp.test(message)){
					code = index;
					msg = message.replace(exp,'');
				}
			}
		}
		return {message:msg,code:code};
	}
	this.checkCode = function(message){
		var codes = {
		    normal: '\\u0001'
		},exp,code;
		for(var index in codes){
			if(codes.hasOwnProperty(index)){
				exp = new RegExp('('+codes[index]+')');
				if(exp.test(message)){
					code = null;
					message = message.replace(exp,'');
					if(message.indexOf('VERSION')!==-1){
						message = message.replace(/VERSION/g,version);
						code = 'version';
					}
					if(message.indexOf('ACTION')!==-1){
						message = message.replace(/ACTION/g,'');
						code = 'emote';
					}
					return {message:message,code:code};
				}
			}
		}
		return {message:message,code:false};
	}
	this.checkClientCode = function(message){
		var codes = {
			emote: '\/me '
		},exp,code;
		for(var index in codes){
			if(codes.hasOwnProperty(index)){
				exp = new RegExp('('+codes[index]+')');
				if(exp.test(message)){
					code = index;
					message = message.replace(exp,'');
					return {message:message,code:code};
				}
			}
		}
		return {message:message,code:false};
	}
}