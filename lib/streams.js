var fs = require('fs'),
	request = require('request'),
	conf = __dirname+'/../config/';
module.exports=function(io,callback){
	var fn = this,
		main,user,current,
		index=0,i=0;
	fs.readFile(conf+'main.json','utf8',function(err,data){
		if(err){ 
    		console.log('Error: '+err);
    		return;
  		}
  		data = JSON.parse(data);
  		main = data;
  		init();
	});
	io.sockets.on('connection',function(socket){
		socket.emit('all_streams',main);
		callback(socket);
	});
	function parseStreamCallback(stream,channel,status){
		if(!main[stream].channels[channel].online && !main[stream].channels[channel].viewers){
			main[stream].channels[channel].online = false;
			main[stream].channels[channel].viewers = 0;
		}
		if(main[stream].channels[channel].online !== status.online){
			main[stream].channels[channel].online = status.online;
			main[stream].channels[channel].viewers = status.viewers;
			//send message to client of change
			io.sockets.emit('stream_update',{
				stream:stream,
				channel:channel,
				status:status
			});
		}
	}
	function checkLivestream(stream,channel){
		var online_url = 'http://x'+main[stream].channels[channel].channel+'x.api.channel.livestream.com/2.0/livestatus.json',
			status = {};
		request({
			url:online_url,
			headers:{
				'Host':'x'+main[stream].channels[channel].channel+'x.api.channel.livestream.com'
			}
		},function(error,response,body){
			if(!error && response.statusCode == 200){
				body = JSON.parse(body);
				if(body.channel.isLive){
					status.online = true;
					status.viewers = body.channel.currentViewerCount;
					parseStreamCallback(stream,channel,status);
				}else{
					status.online = false;
					status.viewers = 0;
					parseStreamCallback(stream,channel,status);
				}
			}else{
				status.online = false;
				status.viewers = 0;
				parseStreamCallback(stream,channel,status);
			}
		});
	}
	function checkPicarto(stream,channel){
		var online_url = 'https://www.picarto.tv/live/ajaxOnlineCheck.php?channel='+main[stream].channels[channel].channel,
			viewer_url = 'https://www.picarto.tv/live/ajaxviewer.php?channel='+main[stream].channels[channel].channel,
			status = {};
		request.get(online_url,function(error,response,body){
			if(!error && response.statusCode == 200){
				if(body==1){
					status.online = true;
					request.get(viewer_url,function(error,response,body){
						status.viewers = parseInt(body);
						parseStreamCallback(stream,channel,status);
					});
				}else{
					status.online = false;
					status.viewers = 0;
					parseStreamCallback(stream,channel,status);
				}
			}else{
				status.online = false;
				status.viewers = 0;
				parseStreamCallback(stream,channel,status);
			}
		});
	}
	function checkTwitch(stream,channel){
		var online_url = 'https://api.twitch.tv/kraken/streams/'+main[stream].channels[channel].channel,
			status = {};
		request.get(online_url,function(error,response,body){
			if(!error && response.statusCode == 200){
				body = JSON.parse(body);
				if(body.stream!==null){
					status.online = true;
					status.viewers = body.stream.viewers;
					parseStreamCallback(stream,channel,status);
				}else{
					status.online = false;
					status.viewers = 0;
					parseStreamCallback(stream,channel,status);
				}
			}else{
				status.online = false;
				status.viewers = 0;
				parseStreamCallback(stream,channel,status);
			}
		});
	}
	function init(){
		setInterval(function(){
			switch(main[index].channels[i].type){
				case 'livestream':
					checkLivestream(index,i);
					break;
				case 'twitch':
					checkTwitch(index,i);
					break;
				case 'picarto':
					checkPicarto(index,i);
					break;
			}
			if((index+1)==main.length){
				if((i+1)==main[index].channels.length){
					index=0;
					i=0;
				}else{
					i++;
				}
			}else{
				if((i+1)==main[index].channels.length){
					i=0;
					index++;
				}else{
					i++;
				}
			}
		},10000);
	}
}