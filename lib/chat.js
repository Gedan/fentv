var irc = require('irc'),
	func = require('./func.js'),
	version = '0.0.1', //fenchat version
	func = new func(version);
module.exports=function(socket){
	var fn = this;
	this.conn = false;
	socket.on('join-fenchat',function(data){
		fn.conn = new irc.Client('irc.rizon.net', data.nick, {
		    channels: ['#fenchat'],
		    autoConnect: false
		});
		fn.conn.connect(function(){
			socket.emit('fenchat-connected',{
				nick:data.nick
			});
		});
		fn.conn.addListener('names',function(channel,nicks){
			socket.emit('channel-listing',nicks);
		});
		fn.conn.addListener('part',function(channel,nick,reason,message){
			socket.emit('fenchat-message',{type:'part',nick:nick,reason:reason,message:message});
		});
		fn.conn.addListener('kick',function(channel,nick,by,reason,message){
			socket.emit('fenchat-message',{type:'kick',nick:nick,reason:reason,by:by,message:message});
		});
		fn.conn.addListener('nick',function(oldnick,newnick,channels,message){
			socket.emit('fenchat-message',{type:'nick',oldnick:oldnick,newnick:newnick,message:message});
		});
		fn.conn.addListener('raw',function(message){
			//console.log(message);
			var color,code;
			switch(message.command){
				case 'PRIVMSG':
					color = func.getColor(message.args[1]);
					code = func.checkCode(color.message);
					console.log(code);
					socket.emit('fenchat-message',{
						type:(code.code=='emote'?'emote':'message'),
						nick:message.nick,
						to:message.args[0],
						text:code.message,
						color:color.code,
						code:code.code
					});
					break;
				case 'JOIN':
					socket.emit('fenchat-message',{
						type:'join',
						nick:message.nick,
						reason:message.args[0].replace(/quit: /g,'')
					});
					break;
				case 'QUIT':
					socket.emit('fenchat-message',{
						type:'quit',
						nick:message.nick,
						to:message.args[0]
					});
					break;
				case 'NOTICE':
					// socket.emit('fenchat-message',{
					// 	type:'notice',
					// 	nick:message.nick,
					// 	to:message.args[0],
					// 	text:message.args[1]
					// });
					break;
			}
		});
		fn.conn.addListener('error', function(message) {
		    console.log('error: ', message);
		});
	});
	socket.on('fenchat-message',function(data){
		var parse = func.checkClientCode(data.msg);
		if(parse.code=='emote'){
			fn.conn.action('#fenchat',parse.message);
		}else{
			fn.conn.say('#fenchat',data.msg);
		}
	});
}