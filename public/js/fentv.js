var main=false;
function triggerHover(){
	jQuery('.stream').each(function(){
		jQuery(this).off('mouseover').off('mouseout').on('mouseover',function(){
			var stream = jQuery(this).attr('data-stream');
			var toggleHtml = '',toggleHtml2 = '',
				image = '/img/offline.jpg';
			toggleHtml += '<div class="stream-info">';
			for(var i=0;i<main[stream].channels.length;i++){
				toggleHtml2 += '<strong>'+main[stream].channels[i].type+':</strong> '+(main[stream].channels[i].viewers?main[stream].channels[i].viewers:0)+'<br>';
				if(main[stream].channels[i].online){
					//if online get proper image
					switch(main[stream].channels[i].type){
						case 'livestream':
							image = 'http://thumbnail.api.livestream.com/thumbnail?name='+main[stream].channels[i].channel+'&t='+new Date().valueOf();
							break;
					}
				}
			}
			//todo, create default offline image and pull in proper stream image
			toggleHtml += '<img class="info-img" src="'+image+'">';
			toggleHtml += '<div class="info-right"><strong>'+main[stream].name+'</strong><br>';
			toggleHtml += toggleHtml2;
			toggleHtml += '</div></div>';
			jQuery(this).append(toggleHtml);
		}).on('mouseout',function(){
			jQuery(this).find('.stream-info').remove();
		});
	});
}
socket.on('stream_update',function(data){
	if(main!==false){
		if(!main[data.stream].channels[data.channel].online && !main[data.stream].channels[data.channel].viewers){
			main[data.stream].channels[data.channel].online = false;
			main[data.stream].channels[data.channel].viewers = 0;
		}
		if(main[data.stream].channels[data.channel].online !== data.status.online){
			main[data.stream].channels[data.channel].online = data.status.online;
			main[data.stream].channels[data.channel].viewers = data.status.viewers;
		}
	}
	jQuery('.stream[data-stream="'+data.stream+'"] .status').html((data.status.online===true?'online':'offline')+' '+data.status.viewers==0?'':data.status.viewers);
	triggerHover();
});
socket.on('all_streams',function(data){
	main = data;
	streamsHtml = '';
	for(var i=0;i<main.length;i++){
		var online = false, viewers = 0;
		for(var ii=0;ii<main[i].channels.length;ii++){
			if(main[i].channels[ii].online===true){
				online = true;
				viewers += main[i].channels[ii].viewers;
			}
		}
		streamsHtml+='<div class="stream" data-stream="'+i+'"><div class="status">'+(online?'online':'offline')+' '+(viewers==0?'':viewers)+'</div>'+main[i].name+'</div>';
	}
	jQuery('.list-body .main-body').html(streamsHtml);
	triggerHover();
});